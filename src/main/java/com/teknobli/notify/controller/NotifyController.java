package com.teknobli.notify.controller;

import com.teknobli.notify.dto.StockNotifyDTO;
import com.teknobli.notify.entity.StockNotify;
import com.teknobli.notify.services.NotifyService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/notify")
public class NotifyController {

    @Autowired
    NotifyService notifyService;

    @PostMapping("/product/stockNotification")
    public StockNotifyDTO productStockNotify(@RequestBody StockNotifyDTO stockNotifyDTO){
        StockNotifyDTO stockNotifyDTOCreated = new StockNotifyDTO();
        StockNotify stockNotifyCreated = notifyService.saveStockNotify(stockNotifyDTO);
        if(stockNotifyCreated == null)
            BeanUtils.copyProperties(stockNotifyCreated,stockNotifyDTOCreated);
        return stockNotifyDTOCreated;
    }

    @GetMapping("/product/updateStock/{productId}")
    public void updateStock(@PathVariable("productId") String productId){
        notifyService.updateStock(productId);
    }

}
