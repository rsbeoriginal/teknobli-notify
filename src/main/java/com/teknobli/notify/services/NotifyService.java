package com.teknobli.notify.services;

import com.teknobli.notify.dto.StockNotifyDTO;
import com.teknobli.notify.entity.StockNotify;

public interface NotifyService {
    StockNotify saveStockNotify(StockNotifyDTO stockNotifyDTO);

    void updateStock(String productId);
}
