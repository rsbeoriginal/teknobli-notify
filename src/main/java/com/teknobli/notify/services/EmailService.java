package com.teknobli.notify.services;

public interface EmailService {

    void sendSimpleMessage(String to, String subject, String text);
}
