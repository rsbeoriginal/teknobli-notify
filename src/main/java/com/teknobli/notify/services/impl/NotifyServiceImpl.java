package com.teknobli.notify.services.impl;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import com.teknobli.notify.dto.StockNotifyDTO;
import com.teknobli.notify.entity.StockNotify;
import com.teknobli.notify.productmicroservice.Endpoints;
import com.teknobli.notify.productmicroservice.dto.Product;
import com.teknobli.notify.repository.StockNotifyRepository;
import com.teknobli.notify.services.EmailService;
import com.teknobli.notify.services.NotifyService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
public class NotifyServiceImpl implements NotifyService {

    @Autowired
    StockNotifyRepository stockNotifyRepository;

    @Autowired
    EmailService emailService;


    @Override
    public StockNotify saveStockNotify(StockNotifyDTO stockNotifyDTO) {
        if (stockNotifyRepository.checkStockNotify(stockNotifyDTO.getProductId(),stockNotifyDTO.getUserId()) == 0) {
            StockNotify stockNotify = new StockNotify();
            BeanUtils.copyProperties(stockNotifyDTO,stockNotify);
            if(stockNotify.getFcmToken()==null || stockNotify.getFcmToken().trim().length()==0 || stockNotify.getFcmToken().equalsIgnoreCase("nil")){
                stockNotify.setFcmToken("NIL");
            }
            return stockNotifyRepository.save(stockNotify);
        }
        return null;
    }

    @Override
    public void updateStock(String productId) {
        System.out.println("notify: " + productId);
        Product product= getProduct(productId);
        if(product!=null) {
            List<StockNotify> stockNotifyList = stockNotifyRepository.getSubscribers(productId);
            for (StockNotify stockNotify : stockNotifyList) {
                try {
                    String emailTo = FirebaseAuth.getInstance().getUser(stockNotify.getUserId()).getEmail();
                    String fcmToken = stockNotify.getFcmToken();
                    sendMailInThread(emailTo,product);
                    if(!fcmToken.equals("nil"))
                        sendPushNotification(fcmToken,product);
                    stockNotifyRepository.delete(stockNotify.getNotifyId());
                } catch (FirebaseAuthException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private Product getProduct(String productId) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(Endpoints.BASE_URL+Endpoints.GET_SINGLE_PRODUCT_DETAILS+productId,Product.class);
    }

    private void sendPushNotification(String fcmToken,Product product) {
        Message message = Message.builder()
                .setNotification(new Notification("Hooray, your product is back in stock",product.getProductName() + "is now available"))
                .putData("productId",product.getProductId())
                .setToken(fcmToken)
                .build();
        try {
            FirebaseMessaging.getInstance().send(message);
        } catch (FirebaseMessagingException e) {
            e.printStackTrace();
        }
    }

    private void sendMailInThread(String emailTo, Product product) {
        Thread mailThread = new Thread(new MailThread(emailTo,"Hooray!!! " + product.getProductName() +" is in stock",
                product.getProductName()+" for which you subscribed is back in stock!!\n\nPlease buy early to avoid out of stock problem.",emailService));
        mailThread.start();
    }

    public class MailThread implements Runnable{

        String emailTo;
        String subject;
        String text;

        EmailService emailService;

        public MailThread(String emailTo, String subject, String text, EmailService emailService) {
            this.emailTo = emailTo;
            this.subject = subject;
            this.text = text;
            this.emailService = emailService;
        }

        @Override
        public void run() {
            emailService.sendSimpleMessage(emailTo,subject,text);
            System.out.println("mail send: " +emailTo);
        }
    }

}
