package com.teknobli.notify.repository;

import com.teknobli.notify.dto.StockNotifyDTO;
import com.teknobli.notify.entity.StockNotify;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StockNotifyRepository extends CrudRepository<StockNotify,String> {

    @Query(value = "SELECT COUNT(*) FROM StockNotify WHERE (productId =?1 AND userId=?2)")
    Integer checkStockNotify(String productId,String userId);

    @Query(value = "FROM StockNotify  WHERE productId = ?1")
    List<StockNotify> getSubscribers(String productId);
}
